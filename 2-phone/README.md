# Mobile phone datalogging

An Android mobile phone provides a WiFi hotspot to which the instrument modules connect. Data is recorded on the phone and displayed in a basic manner to enable verfication of functionality.
Code for this Android app is developed separately, the latest release can be downloaded from: https://github.com/Future-Hangglider/Hanglog3/releases
